#Lepbase workshop
Code to generate interactive perl tutorials (using AJAX and CGI)

##setup
* `nano /path/to/http.conf` and uncomment the line `LoadModule cgi_module modules/mod_cgi.so`
* `sudo apachectl restart` or equivalent command to restart apache
* Make sure you have the `JSON` perl module installed in a location that is in the `PERL5LIB` of the http user
* Copy files to a location where they can be served as web pages (e.g. `/Library/WebServer/Documents` on a Mac).
* `cd lepbase-workshop/pl`
* `nano prepare.pl` and set `$js_dir` and `$cgi_dir` as required
* `nano request_handler.pl` and edit as required 
	* the subs in this file will be turned into sections on the web page and executed when the user presses submit
	* only subs listed in the dispatch table can be executed in resopnse to a cgi request so edit `$dispatch` accordingly 

##usage
* `perl prepare.pl request_handler.pl` reads the subs in `request_handler.pl` and generates a javascript file `../js/tutorial_code.js` which is used by `API_interface.js` to generate the web interface
* go to `http://localhost/lepbase-workshop/index.html` (or equivalent url if installed elsewhere)

## syntactical conventions
Only code in subs will be added to the web page. To control whih parts of a sub are shown use the following conventions:

* `##s` = don't print this line (skip)
* `##-` = don't print the comment
* `##+` = uncomment before printing
* `##x` = stop parsing sub (use on first line to skip a sub)

`shift` will be replaced with a text input any time it matches the pattern:

 `/^\s*my\s+(\$)(\w+)\s=\sshift/`.
 
As in:

`my $var = shift;`

To control the default text in the textbox use the pattern:

`my $var = shift; ##- value=initial value;`

To pass variables to a sub that cannot be set by the user, just break the typical pattern, e.g.:

`my ($var) = (shift);`
`my $var = $_[0]; shift;`

 