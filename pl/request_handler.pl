#!/usr/bin/perl

use strict;
use warnings;
use CGI qw(:standard);
use JSON;

my $q = CGI->new();
my %response;
my $json;
my @params = $q->param;
print $q->header('application/json');
my @vars;
foreach my $key (@params){
	$response{$key} = $q->param($key); #( 'success' => 'true' );
	if ($key ne ' heading'){
		push @vars,$q->param($key);
	}
	else {
		unshift @vars,$q->param($key)
	}
}

my $dispatch = {	API_connection => \&API_connection,
					database_adaptor => \&database_adaptor,
					half_sub => \&half_sub,
					hidden_sub => \&hidden_sub };

if ($response{'heading'}){
	print STDERR $response{'heading'},"\n";
	$json = to_json $dispatch->{$response{'heading'}}->(@vars);
}
else {
	$json = "{'ERROR':'unable to run code'}";
}

print $json;

sub API_connection {
	my ($sub) = (shift);
	##+ return; # this won't happen ##- and this won't print
	my $variable = shift; ##- pattern=text; value=a text string;
	my %response = ( 'heading' => $sub, variable => $variable );
	return \%response;
}

sub database_adaptor {
	my ($sub) = (shift);
	my $first = shift; ##- pattern=text; value=the initial string;
	my $second = shift; ##- pattern=text; value=a subsequent string;
	my %response = ( 'heading' => $sub, first => $first, second => $second );
	my @arr = qw ( elephant emperor hummingbird );
	push @arr,"death's head";
	my %hash = ( 'classification' => 'hawk moths' );
	$hash{'names'} = \@arr;
	$response{'hash'} = \%hash;
	return \%response;
}

sub hidden_sub {
	##x This sub won't be added to the web page 
	my ($sub) = (shift);
	my $first = shift; ##- pattern=text; value=the initial string;
	my $second = shift; ##- pattern=text; value=a subsequent string;
	my %response = ( 'heading' => $sub, first => $first, second => $second );
	my @arr = qw ( elephant emperor hummingbird );
	push @arr,"death's head";
	$response{'array'} = \@arr;
	return \%response;
}


sub half_sub {
	my ($sub) = (shift);
	my $first = shift; ##- pattern=text; value=the initial string;
	my $second = shift; ##- pattern=text; value=a subsequent string;
	##x The rest of this sub won't be added to the web page 
	my %response = ( 'heading' => $sub, first => $first, second => $second );
	my @arr = qw ( elephant emperor hummingbird );
	push @arr,"death's head";
	$response{'array'} = \@arr;
	return \%response;
}

