#!/usr/bin/perl -w

use strict;
use JSON;
use Cwd 'abs_path';
use File::Basename;

# set the directories to write files to
my $js_dir = '/Library/WebServer/Documents/tutorials/lepbase-workshop/js';
my $cgi_dir = '/Library/WebServer/CGI-Executables';

# find the full path to the directory that this script is executing in
my $perl_dir = dirname(abs_path($0));

# read subs and generate js file,
# deploy request_handler to cgi_dir
# write js representation to js_dir

my $heading;
my @sub;
my @arr;
while (my $line = <>){
	chomp $line;
	if ($line =~ m/^sub\s+(\w+)/){
		while (@sub){
			my $last = pop @sub;
			next unless $last =~ m/\}/;
			if (@sub){
			my $hashref = sub_to_hashref($heading,\@sub);
				push @arr,$hashref if $hashref->{'perl'};
			}
			@sub = ();
		}
		$heading = $1;
	}
	else {
		push @sub,$line if $heading;
	}
}
while (@sub){
	my $last = pop @sub;
	next unless $last =~ m/\}/;
	if (@sub){
		my $hashref = sub_to_hashref($heading,\@sub);
		push @arr,$hashref if $hashref->{'perl'};
	}
	@sub = ();
}

my $json = prepare_js(\@arr);
open OUT,">$js_dir/tutorial_code.js";
print OUT "var code = $json;\n";

system "cp $perl_dir/request_handler.pl $cgi_dir/request_handler.pl";

sub sub_to_hashref {
	my $heading = shift; ##- pattern=text; value=suitable heading;
	my $sub = shift; ##- value=example string;
	my @perl;
	my %data;
	my @vars;
	while (my $line = shift @$sub){
		last if $line =~ m/\s+##x/; # do not return any more of this sub (use on first line to skip sub)
		next if $line =~ m/\s+##s/; # do not return line (skip)
		if ($line =~ m/^\s*my\s+(\$)(\w+)\s=\sshift/){
			my $type = $1;
			my $name = $2;
			my $placeholder = '__'.$name.'__';
			$line =~ s/shift/$placeholder/;
			my %var_hash;
			$var_hash{name} = $type.$name;
			$var_hash{placeholder} = $placeholder;
			if ($line =~ s/\s+##-\s*(.*)//){
				my $extra = $1;
				if ($extra =~ m/pattern=([^;]+)/){
					$var_hash{pattern} = $1;
				}
				if ($extra =~ m/value=([^;]+)/){
					$var_hash{value} = $1;
				}
			}
			push @vars,\%var_hash;
		}
		$line =~ s/\s+##-.*//; # remove comment from end of line before returning
		$line =~ s/##\+\s*//; # uncomment line before returning (appear to add code that won't be executed)
		push @perl,$line;
	}
	my %hash;
	return \%hash unless @perl;
	$hash{heading} = $heading;
	$hash{perl} = \@perl;
	$hash{vars} = \@vars if @vars;
	return \%hash;
}

sub prepare_js {
	my $array_ref = shift;
	my $json = to_json $array_ref;
	$json =~ s/("perl":\[.+?]),("\w)/$1.join\("<br\/>\\n"\),$2/g;
	return $json;
}