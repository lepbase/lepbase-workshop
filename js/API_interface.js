
var ajax_url = '/cgi-bin/request_handler.pl';
//var code = [{"perl":["\tmy $heading = __heading__;","\tmy $sub = __sub__;","\tmy @perl;","\tmy %data;","\tmy @vars;","\twhile (my $line = shift @$sub){","\t\tif ($line =~ m/^\\s*my\\s+(\\$)(\\w+)\\s=\\sshift/){","\t\t\tmy $type = $1;","\t\t\tmy $name = $2;","\t\t\tmy $placeholder = '__'.$name.'__';","\t\t\t$line =~ s/shift/$placeholder/;","\t\t\tmy %var_hash;","\t\t\t$var_hash{name} = $type.$name;","\t\t\t$var_hash{placeholder} = $placeholder;","\t\t\tif ($line =~ s/\\s*##!(.*)//){","\t\t\t\tmy $extra = $1;","\t\t\t\tif ($extra =~ m/pattern=([^;]+)/){","\t\t\t\t\t$var_hash{pattern} = $1;","\t\t\t\t}","\t\t\t\tif ($extra =~ m/value=([^;]+)/){","\t\t\t\t\t$var_hash{value} = $1;","\t\t\t\t}","\t\t\t}","\t\t\tpush @vars,\\%var_hash;","\t\t}","\t\tpush @perl,$line;","\t}","\tmy %hash;","\t$hash{heading} = $heading;","\t$hash{perl} = \\@perl;","\t$hash{vars} = \\@vars;","\treturn \\%hash;"].join("<br/>\n"),"heading":"sub_to_hashref","vars":[{"pattern":"text","value":"suitable heading","name":"$heading","placeholder":"__heading__"},{"name":"$sub","placeholder":"__sub__"}]},{"perl":["\tmy $array_ref = __array_ref__;","\tmy $json = to_json $array_ref;","\t$json =~ s/(\"perl\":\\[.+?]),(\"\\w)/$1.join\\(\"<br\\/>\\\\n\"\\),$2/g;","\treturn $json;"].join("<br/>\n"),"heading":"prepare_js","vars":[{"name":"$array_ref","placeholder":"__array_ref__"}]}];


(function($) {
     $.fn.textWidth = function () {
     $body = $('body');
     $this =  $(this);
     $text = $this.text();
     if($text=='') $text = $this.val();
     $text = $text.replace(/\s/g,'_');
     var calc = '<div style="clear:both;display:block;visibility:hidden;"><span style="width;inherit;margin:0;font-family:'  + $this.css('font-family') + ';font-size:'  + $this.css('font-size') + ';font-weight:' + $this.css('font-weight') + '">' + $text + '</span></div>';
     $body.append(calc); 
     var width = $('body').find('span:last').width(); 
      $body.find('span:last').parent().remove(); 
     return width; 
    };
 })(jQuery);


var output_area_div = d3.select('body').append('div').attr('id','output_area').attr('class','output_area');
var output_div = output_area_div.append('div').attr('id','output').attr('class','output');
var input_area_div = d3.select('body').append('div').attr('id','input_area').attr('class','input_area');
var input_div = input_area_div.append('div').attr('id','input').attr('class','input');
var stage_div = input_div.selectAll('div').data(code).enter().append('div');
stage_div.attr('id',function(d,i){ return 'stage_' + i; })
    .attr('class','stage');
var head_div = stage_div.append('div').attr('class','head');
head_div.on('click',function(){
	var div = d3.select(this.parentNode);
    if (div.classed('expanded')){
    	div.classed('expanded',false);
    }
    else {
      div.classed('expanded',true);
    }
    $(div[0]).find('input').each(function(i, block){
        var text_width =  $(block).textWidth();
		$(block).width(text_width);
  	  });
  });
head_div.append('h2').text(function(d){ return d.heading });
var perl_div = stage_div.append('div').attr('class','pl');
var perl_span = perl_div.append('span').attr('class','code perl');
perl_span.html( function(d){
	var perl = d.perl.replace(/\t/g,'&nbsp;&nbsp;&nbsp;&nbsp;');
	d.vars.forEach(function(e){
	    var input = '<input class="resizable hljs-string" type="text" rel="' + e.name + '" value="' + e.value + '"/>';
	    perl = perl.replace(e.placeholder,input);
	  });
    return perl;
    
  });
//var send_div = stage_div.append('div').attr('class','send');
var submit_div = stage_div.append('div').attr('class','submit').text('submit');
submit_div.on('click',function(d){
    var values = {};
    values.heading = d.heading;
    var elems = $(this).parent().find(':input').toArray();
    var count = elems.length;
    elems.forEach(function(item){
        var name = $(item).attr('rel');
	    values[name] = $(item).val();
	    if (!--count) call_ajax(values);
	  });
	
  });

function call_ajax (values){
  $.ajax({
        type: 'POST',
        url: ajax_url,
        dataType: 'json',
        data: values,
        success: function(data){
            var html = JSON.stringify(data,null,2);
            html = html.replace(/\n/g,'<br/>');
            html = html.replace(/\s/g,'&nbsp;');
            d3.select('#output').html(html);
            $('.output').each(function(i, block) {
    			hljs.highlightBlock(block);
  			});
        },
        error: function(error){
        	console.log('!');
            console.log(error);
        },
        complete: function() {
          //restore button
        }
    });
}

$('span.code').each(function(i, block) {
    hljs.highlightBlock(block);
  });


$('.resizable').on("propertychange change keyup paste input", function(e){
	var text_width =  $(e.target).textWidth();
	$(e.target).width(text_width);
  });

/* 
 * select all text on focus
$('.resizable').focus(function (event) {
    $(event.target).select().one('mouseup', function (e) { 
        e.preventDefault(); 
      }); 
  });
*/

